---
title: "Blogs da comunidade"
kind: page
created_at: 2020-09-13 20:09
author: Paulo Henrique de Lima Santana
---

# Blogs da comunidade

GUD - RS

<http://softwarelivre.org/debian-rs>

GUD - BA

<http://softwarelivre.org/gud-ba/>

Debian Maníaco

<http://debianmaniaco.blogspot.com>


João Eriberto Mota Filho

<http://eriberto.pro.br/blog>

