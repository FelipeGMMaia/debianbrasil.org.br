---
title: "Proposta para Grupo de Usuários Debian no FISL 15"
kind: article
created_at: 2014-03-09 22:26
author: Andre Felipe Machado
---

O Grupo de Usuários Debian Brasil utilizará o espaço de usuários no FISL15
(em 2014) para atividades:


-   divulgar o Projeto Debian
-   divulgar Grupos de Usuários Debian em vários Estados.
-   congregar usuários da distribuição GNU / Linux, GNU / kFreeBSD, GNU / Hurd.
-   concentração de usuários, palestrantes, divulgadores durante o FISL 15.
-   troca de informações técnicas.
-   auxiliar em dúvidas de instalação.
-   planejar ações de divulgação e incentivo ao uso de Debian.

Participantes  já confirmados no espaço de usuários.

Estes e mais participantes colocarão seus nomes como comentários neste texto
para poderem ser contatados pela organização:

- André Felipe Machado, afmachado1963 EM gmail.com
- Luiz Eduardo Guaraldo
- Hélio Loureiro
- Flávio Menezes dos Reis,  flaviomreis EM gmail.com
- Rafael Ivanir Costa Oliveira,  tecnicorafael.oliveira EM Gmail.com
