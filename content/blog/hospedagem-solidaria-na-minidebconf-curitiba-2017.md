---
title: "Hospedagem Solidária na MiniDebConf Curitiba 2017"
kind: article
created_at: 2017-01-11 11:35
author: Daniel Lenharo de Souza
---

A Comunidade Debian, quer dar uma ajuda para quem precisa de um cantinho para
ficar na [MiniDebConf Curitiba 2017](http://br2017.mini.debconf.org/),
nos dias 17, 18 e 19 de março.

Assim, criamos 2 tópicos no fórum para que você possa oferecer ou pedir um
quarto (cama, sofá, etc.) durante a MiniDebConf.

Não há coisa melhor que aproveitar um evento próximo à pessoas que compartilham
dos mesmo valores!!!

Obs: O fórum foi desativado.
