---
title: "Debian Day 30 anos em Belo Horizonte"
kind: article
created_at: 2023-08-24 23:00
author: Paulo Henrique de Lima Santana (phls)
---

Pela primeira vez a cidade de Belo Horizonte realizou um
[Debian Day](https://wiki.debian.org/DebianDay/2023) para celebrar o
aniversário do [Projeto Debian](https://www.debian.org).

As comunidades [Debian Minas Gerais](https://debian-minas-gerais.gitlab.io/site)
e [Software Livre de BH e Região](https://softwarelivre.bhz.br/)
se sentiram motivadas para celebrar esta data especial devido aos 30 anos do
Projeto Debian em 2023 e organizou um encontro no dia 12 de agosto dentro
[Espaço do Conhecimento da UFMG](https://www.ufmg.br/espacodoconhecimento/descubra/sala-multiuso/).

A organização do Debian Day em Belo Horizonte recebeu o importante apoio do
[Departamento de Ciência da Computação da UFMG](https://dcc.ufmg.br) para
reservar a sala que foi utilizada para o evento.

A programação contou com três atividades:

 - Palestra O projeto Debian quer você! Paulo Henrique de Lima Santana
 - Palestra Personalizando o Debian para uso em escolas da PBH: a história da Libertas - Fred Guimarães
 - Bate-papo sobre os próximos passos para formar uma comunidade de Software Livre em BH - Bruno Braga Fonseca

No total etiveram presentes 11 pessoas e fizemos uma foto com as que ficaram
até o final.

![Presentes no Debian Day 2023 em BH](/blog/imagens/debian30-belo-horizontel-phls_2023-08-12.jpg =400x)
